#### Notes for "Module 8 - Build Automation & CI with Jenkins" exercises

EXERCISE 1: Dockerize your NodeJS App

See `Dockerfile`.

EXERCISE 2: Create a full pipeline for your NodeJS App

See `Jenkinsfile`.

EXERCISE 3: Manually deploy new Docker Image on server

- If build does not succeed b/c Docker image fails to build, as a first troubleshooting point, try to build the image locally.

- If build succeeds, pull latest image from DockerHub private repo and run on the droplet:
  - `echo [password] | docker login -u [username] --password-stdin`
  - `docker run -d -p 3000:3000 upnata/module-8-bootcamp-node-project:[latest image tag]`
    - Host port can be anything (but must not be running another process). Container port must be 3000 b/c that's where the app's Express server is running.
  - Go to `[dropletIPv4]:3000` to confirm built image works.
    - If server is not running:
      - !!! Check droplet's firewall settings to ensure port is open to incoming traffic !!!
      - Try to build image locally and run on localhost. If it works, it's a server prob.
      - Troubleshoot by entering container terminal: `docker exec -it [container_hash] /bin/sh`

EXERCISE 4: Extract all logic into [Jenkins Shared Library](https://gitlab.com/twndevops/jenkins-exercises-shared-library) with parameters and reference it in Jenkinsfile

- As a reference, here is the [Jenkinsfile](https://gitlab.com/twndevops/jenkins-exercises/-/blob/8a2b276e9364e6d8b546c82bd12dd9e2081404e5/Jenkinsfile) before refactoring to use Shared Library.
- None of the functions take parameters.

MISC NOTES:

- If Jenkinsfile contains invalid syntax (confirm in Build's Console Output), you cannot update the file in the Build Replay.
- Valid tool types:  
  ant, hudson.tasks.Ant$AntInstallation  
  git, hudson.plugins.git.GitTool  
  gradle, hudson.plugins.gradle.GradleInstallation  
  jdk, hudson.model.JDK  
  jgit, org.jenkinsci.plugins.gitclient.JGitTool  
  jgitapache, org.jenkinsci.plugins.gitclient.JGitApacheTool  
  maven, hudson.tasks.Maven$MavenInstallation  
  nodejs, jenkins.plugins.nodejs.tools.NodeJSInstallation

- Pull version updates after pipeline builds! Use `git pull -r` to stack your changes on top before pushing.
