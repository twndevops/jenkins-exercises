#!/user/bin/env groovy

// annotation to import shared lib
@Library('jenkins-exercises-shared-library')_

pipeline {
  // Terminology: agent and stages are sections, tools is a directive
  agent any

  tools {
    nodejs 'nodejs-21.2.0'
  }

  stages {

    stage("run app tests") {
      steps {
        script {
          // Reference: https://www.jenkins.io/doc/pipeline/steps/workflow-durable-task-step/#dir-change-current-directory
          // sh "cd app" does NOT work! By default, "npm i" executes in pipeline workspace dir: /var/jenkins_home/workspace/jenkins-exercises-pipeline_main/.
          dir("app") {
            // calling testApp global var (testApp.groovy) in shared lib
            testApp()
          }
        }
      }
    }

    // increments version in Jenkins' locally checked out repo, not your local package.json!
    stage("increment app version") {
      steps {
        script {
          dir("app") {
            incrementVersion()
          }
        }
      }
    }

    // Nana's solution skips this stage
    // stage("build TAR") {
    //   steps {
    //     script {
    //       dir("app") {
    //         echo "Building TAR..."
    //         // if a prev version TAR file exists, remove it so Dockerfile's "docker build" has only 1 arg
    //         // if no prev version exists, rm throws error so rest of stages are skipped
    //         // References: https://sentry.io/answers/determine-whether-a-file-exists-or-not-in-bash/, https://www.howtogeek.com/858815/linux-rm-command/
    //         sh "if test -f bootcamp-node-project-*.tgz; then \n rm bootcamp-node-project-*.tgz \n fi"
    //         // package app into TAR with up-to-date package.json version
    //         sh "npm pack"
    //       }
    //     }
    //   }
    // }

    stage("build image, login to Docker, push to private repo") {
      steps {
        script {
          buildImg()
          dockerLogin()
          pushImg()
        }
      }
    }

    stage("commit version bump") {
      steps {
        script {
          dir("app") {
            gitlabLogin()
            commitVersion()
          }
        }
      }
    }

    // be sure to pull remote package.json version update to your local repo before manually triggering next pipeline build

  }

}